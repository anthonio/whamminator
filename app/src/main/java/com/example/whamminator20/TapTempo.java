package com.example.whamminator20;

import static java.lang.Math.toIntExact;

public class TapTempo {
    private boolean ismeasured = false, istempo = false;
    private long start, stop;
    private double tempo;

    public TapTempo(){
        tempo = 0;
    }

    public int tapTempo(){
        if(stop - start > 9000){
            ismeasured = false;
            istempo = false;
            start = 0;
            stop = 0;
        }else
        if(!ismeasured){
            start = System.currentTimeMillis();
            ismeasured = true;
        }else{
            stop = System.currentTimeMillis();
            ismeasured = false;
            if (!istempo){
                tempo = 60000/toIntExact(stop-start);
                istempo = true; }
            else
                tempo = (60000/toIntExact(stop-start) + tempo)/2;
        }
        return (int)Math.round(tempo);
    }
}
