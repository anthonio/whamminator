package com.example.whamminator20;

import android.content.res.Resources;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;

public class MyBarChart {

    public BarChart EditMyBarChart(BarChart barChart){


        barChart.setScaleEnabled(false);
        barChart.setDragEnabled(false);
        barChart.setTouchEnabled(true);
        barChart.setDrawBarShadow(true);
        barChart.setAutoScaleMinMaxEnabled(false);

        barChart.getAxisLeft().setAxisMaximum(18);
        barChart.getAxisRight().setAxisMaximum(18);
        barChart.getAxisRight().setAxisMinimum(0);
        barChart.getAxisLeft().setAxisMinimum(0);

        barChart.getAxisLeft().setDrawGridLines(false);
        barChart.getAxisLeft().setDrawZeroLine(false);
        barChart.getAxisRight().setDrawZeroLine(false);
        barChart.getAxisRight().setDrawGridLines(false);
        barChart.getXAxis().setDrawGridLines(false);

        barChart.getXAxis().setDrawAxisLine(false);
        barChart.getAxisLeft().setDrawAxisLine(false);
        barChart.getAxisRight().setDrawAxisLine(false);

        barChart.getAxisLeft().setDrawLabels(false);
        barChart.getAxisRight().setDrawLabels(false);
        barChart.getXAxis().setDrawLabels(false);

        Description description = new Description();
        description.setText("");
        barChart.setDescription(description);
        barChart.getLegend().setEnabled(false);

        return barChart;
    }

}
