package com.example.whamminator20;

import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class DTSliderControler {

    private int seqLength=8;
    private int chosenBar=0;
    private int[] seqValuesArray;
    private String[] seqVNamesArray = new String[] {"-O+D","-OCT","-7","-6","-5","-4","-3","-2","-1","OFF","+1","+2","+3","+4","+5","+6","+7","+OCT","+O+D"};

    ArrayList<BarEntry> barEntries = new ArrayList<>(16);

    public int[] fillValuesArray(){
        seqValuesArray=new int[seqLength];
        for (int i=0; i<seqLength; i++){
            seqValuesArray[i]=getChosenValue(i);
        }
        return seqValuesArray;
    }

    public int getSeqLength(){
        return seqLength;
    }

    public void setSeqLength(int seq){
        seqLength=seq;
    }

    public void plusSeqLength(){
        if(seqLength<16){
            seqLength++;
            barEntries.add(new BarEntry(seqLength-1,9));
        }
        else{ seqLength=16; }

    }

    public void minusSeqLength(){
        if(seqLength>1){
            seqLength--;
            barEntries.remove(seqLength);
        }
        else { seqLength=1; }
    }

    public int getChosen(){
        return chosenBar;
    }

    public void setChosen(int chosen){
        chosenBar=chosen;
    }

    public void plusChosen(){
        if (chosenBar<seqLength-1){
            chosenBar++;
        }
        else {chosenBar=0;}
    }

    public void minusChosen(){
        if (chosenBar>0){
            chosenBar--;
        }
        else {chosenBar=seqLength-1;}
    }

    /*public void setChosenValue(int chosen, int value){
        seqValuesArray[chosen]=value;
    }*/

/*
    public int getChosenValue(int chosen){
        return seqValuesArray[chosen];
    }
*/

    public String getSeqValueNames(int value){
        return seqVNamesArray[value];
    }

/*
    public int getChosenValueScaled(int value){
        return (value*50)/9;
    }
*/

    //-------------------------------------------------------------------------//

    public void fillArray(){
        for (int i=0; i<seqLength; i++){
            barEntries.add(new BarEntry(i,9));
        }
    }

    public void setChosenValue(int chosen, int value){
        barEntries.set(chosen,new BarEntry(chosen,value));
    }

    public int getChosenValue(int chosen){
        return (int) barEntries.get(chosen).getY();
    }


}
