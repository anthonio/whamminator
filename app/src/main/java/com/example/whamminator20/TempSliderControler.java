package com.example.whamminator20;

import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class TempSliderControler {

    private int seqLength = 1;
    private int[]  tempEntriesArray;
    ArrayList<BarEntry> barEntries = new ArrayList<>(16);

    public void fillArray(){
        for (int i=0; i<seqLength; i++){
            barEntries.add(new BarEntry(i,this.getChosenValue(i)));
        }
    }

    public int getSeqLength(){
        return seqLength;
    }

    public void setSeqLength(int _seqLength){
        seqLength=_seqLength;
    }

    public void setTempEntriesArray(int[] _entriesArray){
        tempEntriesArray=_entriesArray;
    }

    public int getChosenValue(int chosen){
        return tempEntriesArray[chosen];
    }
}
