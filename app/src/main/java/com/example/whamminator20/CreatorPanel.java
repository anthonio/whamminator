package com.example.whamminator20;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.highlight.Highlight;

public class CreatorPanel extends AppCompatActivity {

    public static final String EXTRA_ENTRIES = "com.example.whamminator20.creatorpanel.EXTRA_ENTRIES";
    public static final String EXTRA_SEQLENGTH = "com.example.whamminator20.creatorpanel.EXTRA_SEQLENGTH";

    DTSliderControler x = new DTSliderControler();
    WHSliderControler y = new WHSliderControler();
    TapTempo tapTempo = new TapTempo();
    private double tempo = 0;
    BarChart barChart;
    BarDataSet barDataSet;
    MyBarChart barChartEdit = new MyBarChart();

    public void refreshBarDataSet_DT(){
        barDataSet.setColor(getResources().getColor(R.color.colorAccent));
        barDataSet.setBarShadowColor(getResources().getColor(R.color.darkgrey));
        barDataSet.setHighLightColor(getResources().getColor(R.color.colorPrimary));
        barDataSet.setHighLightAlpha(255);
        barDataSet.setDrawValues(false);
    }
    public void refreshBarDataSet_WH(){
        barDataSet.setColor(getResources().getColor(R.color.colorPrimary));
        barDataSet.setBarShadowColor(getResources().getColor(R.color.darkgrey));
        barDataSet.setHighLightColor(getResources().getColor(R.color.colorAccent));
        barDataSet.setHighLightAlpha(255);
        barDataSet.setDrawValues(false);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creator_panel);
        final Vibrator vibe = (Vibrator) CreatorPanel.this.getSystemService(Context.VIBRATOR_SERVICE);

        final Button btn_left = (Button)findViewById(R.id.btn_left);
        Button btn_right = (Button)findViewById(R.id.btn_right);
        Button btn_tempo_UP = (Button)findViewById(R.id.btn_tempo_UP);
        Button btn_tempo_DOWN = (Button)findViewById(R.id.btn_tempo_DOWN);
        Button btn_tap_tempo = (Button)findViewById(R.id.btn_tap_tempo);
        final RadioButton radioBtn_DT = (RadioButton)findViewById(R.id.radioBtn_DT);
        final RadioButton radioBtn_WH = (RadioButton)findViewById(R.id.radioBtn_WH);
        final Button btn_live = (Button) findViewById(R.id.btn_live);
        final ToggleButton btn_test = (ToggleButton) findViewById(R.id.btn_test);
        Button btn_minus = (Button)findViewById(R.id.btn_minus);
        Button btn_plus = (Button)findViewById(R.id.btn_plus);
        final TextView text_selected = (TextView) findViewById(R.id.text_Selected);
        final TextView text_seqLength = (TextView) findViewById(R.id.text_SeqLength);
        final SeekBar seekBar_value = (SeekBar) findViewById(R.id.seekBar_value);
        final TextView text_value = (TextView)findViewById(R.id.textView_value);
        Spinner spnr_saved = (Spinner)findViewById(R.id.spnr_saved);
        Spinner spnr_options = (Spinner)findViewById(R.id.spnr_options);
        final EditText tempo_display = (EditText)findViewById(R.id.tempo_display);
        final RadioButton radioBtn_Deep = (RadioButton)findViewById(R.id.radioBtn_Deep);
        final RadioButton radioBtn_Shallow = (RadioButton)findViewById(R.id.radioBtn_Shallow);
        barChart=(BarChart)findViewById(R.id.bar_chart_temp);

        radioBtn_DT.setChecked(true);
        radioBtn_Deep.setChecked(true);

        x.fillArray();
        y.fillArray();

        barDataSet = new BarDataSet(x.barEntries,"");
        BarData theData = new BarData(barDataSet);
        barChart.setData(theData);
        barChart = barChartEdit.EditMyBarChart(barChart);
        barChart.setBackgroundColor(getResources().getColor(R.color.background_color));
        barChart.highlightValue(0,0);

        refreshBarDataSet_DT();


        btn_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);

                if (radioBtn_DT.isChecked()){
                    x.minusChosen();
                    barChart.highlightValue(x.getChosen(),0);
                    text_selected.setText(String.valueOf("Selected: " + (x.getChosen()+1)));
                    text_value.setText(String.valueOf(x.getSeqValueNames(x.getChosenValue(x.getChosen()))));
                    seekBar_value.setProgress(x.getChosenValue(x.getChosen()));
                }
                else {
                    y.minusChosen();
                    barChart.highlightValue(y.getChosen(),0);
                    text_selected.setText(String.valueOf("Selected: " + (y.getChosen()+1)));
                    text_value.setText(String.valueOf(y.getSeqValueNames(y.getChosenValue(y.getChosen()))));
                    seekBar_value.setProgress(y.getChosenValue(y.getChosen()));

                }

            }
        });

        btn_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);

                if(radioBtn_DT.isChecked()) {
                    x.plusChosen();
                    barChart.highlightValue(x.getChosen(),0);
                    text_selected.setText(String.valueOf("Selected: " + (x.getChosen()+1)));
                    text_value.setText(String.valueOf(x.getSeqValueNames(x.getChosenValue(x.getChosen()))));
                    seekBar_value.setProgress(x.getChosenValue(x.getChosen()));

                }
                else{
                    y.plusChosen();
                    barChart.highlightValue(y.getChosen(),0);
                    text_selected.setText(String.valueOf("Selected: " + (y.getChosen()+1)));
                    text_value.setText(String.valueOf(y.getSeqValueNames(y.getChosenValue(y.getChosen()))));
                    seekBar_value.setProgress(y.getChosenValue(y.getChosen()));
                }
            }
        });

        btn_tempo_UP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);


                try {
                    tempo=Integer.parseInt(tempo_display.getText().toString());
                    if(tempo<999) {
                        tempo++;
                        tempo_display.setText(((String.valueOf(Math.toIntExact(Math.round(tempo))))));
                    }
                } catch (NumberFormatException e) {
                    tempo_display.setText("60");
                }


            }
        });

        btn_tempo_DOWN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);


                try {
                    tempo=Integer.parseInt(tempo_display.getText().toString());
                    if(tempo>1) {
                        tempo--;
                        tempo_display.setText(((String.valueOf(Math.toIntExact(Math.round(tempo))))));
                    }
                } catch (NumberFormatException e) {
                    tempo_display.setText("60");
                }
            }
        });

        btn_tap_tempo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);
                tempo = tapTempo.tapTempo();
                if (tempo != 0) {
                    tempo_display.setText(((String.valueOf(Math.toIntExact(Math.round(tempo))))));
                }
            }
        });

        radioBtn_DT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);
                seekBar_value.setMax(18);
                radioBtn_WH.setChecked(false);

                barDataSet = new BarDataSet(x.barEntries,"");
                BarData theData = new BarData(barDataSet);
                barChart.setData(theData);
                barChart = barChartEdit.EditMyBarChart(barChart);
                barChart.setBackgroundColor(getResources().getColor(R.color.background_color));
                barChart.highlightValue(0,0);
                refreshBarDataSet_DT();

                x.setChosen(0);
                x.setSeqLength(x.getSeqLength());
                text_selected.setText(String.valueOf("Selected: " + (x.getChosen()+1)));
                text_seqLength.setText(String.valueOf("Seq Length: " + x.getSeqLength()));
                text_value.setText(String.valueOf(x.getSeqValueNames(x.getChosenValue(x.getChosen()))));




            }
        });

        radioBtn_WH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);
                radioBtn_DT.setChecked(false);
                seekBar_value.setMax(19);

                barDataSet = new BarDataSet(y.barEntries,"");
                BarData theData = new BarData(barDataSet);
                barChart.setData(theData);
                barChart = barChartEdit.EditMyBarChart(barChart);
                barChart.getAxisLeft().setAxisMaximum(19);
                barChart.getAxisRight().setAxisMaximum(19);
                barChart.setBackgroundColor(getResources().getColor(R.color.background_color));
                barChart.highlightValue(0,0);
                refreshBarDataSet_WH();

                y.setChosen(0);
                y.setSeqLength(y.getSeqLength());
                text_selected.setText(String.valueOf("Selected: " + (y.getChosen()+1)));
                text_seqLength.setText(String.valueOf("Seq Length: " + y.getSeqLength()));
                text_value.setText(String.valueOf(y.getSeqValueNames(y.getChosenValue(y.getChosen()))));


            }
        });


        btn_live.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);
                Intent startIntent = new Intent(getApplicationContext(), LivePanel.class);
                if (radioBtn_DT.isChecked()){
                    startIntent.putExtra(EXTRA_ENTRIES,x.fillValuesArray());
                    startIntent.putExtra(EXTRA_SEQLENGTH,x.getSeqLength());
                    startActivity(startIntent);
                }
                else {
                    startIntent.putExtra(EXTRA_ENTRIES,y.fillValuesArray());
                    startIntent.putExtra(EXTRA_SEQLENGTH,y.getSeqLength());
                    startActivity(startIntent);
                }

            }
        });

        btn_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);

                if(btn_test.isChecked()==false){
                    btn_test.setTextColor(getResources().getColor(R.color.darkBlue));
                    btn_test.setBackground(getResources().getDrawable(R.drawable.btn_testtext));
                }
                else {
                    btn_test.setTextColor(getResources().getColor(R.color.brightBlue));
                    btn_test.setBackground(getResources().getDrawable(R.drawable.btn_testtext_pressed));
                }

            }
        });

        btn_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);

                if(radioBtn_DT.isChecked()) {
                    if (x.getSeqLength() > 1) {
                        x.minusSeqLength();
                    }
                    text_seqLength.setText(String.valueOf("Seq Length: " + x.getSeqLength()));

                    if (x.getSeqLength() < x.getChosen()+1) {
                        x.setChosen(x.getSeqLength()-1);
                        text_seqLength.setText(String.valueOf("Seq Length: " + x.getSeqLength()));
                        text_selected.setText(String.valueOf("Selected: " + (x.getChosen()+1)));
                        seekBar_value.setProgress(x.getChosenValue(x.getChosen()));

                    }
                    barDataSet = new BarDataSet(x.barEntries,"");
                    refreshBarDataSet_DT();
                }
                else {
                    if (y.getSeqLength() > 1) {
                        y.minusSeqLength();
                    }
                    text_seqLength.setText(String.valueOf("Seq Length: " + y.getSeqLength()));

                    if (y.getSeqLength() < y.getChosen()+1) {
                        y.setChosen(y.getSeqLength()-1);
                        text_seqLength.setText(String.valueOf("Seq Length: " + y.getSeqLength()));
                        text_selected.setText(String.valueOf("Selected: " + (y.getChosen()+1)));
                        seekBar_value.setProgress(y.getChosenValue(y.getChosen()));
                    }
                    barDataSet = new BarDataSet(y.barEntries,"");
                    refreshBarDataSet_WH();
                }
                BarData theData = new BarData(barDataSet);
                barChart.setData(theData);
                barChart.notifyDataSetChanged();
                barChart.invalidate();
            }
        });

        btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);

                if(radioBtn_DT.isChecked()) {
                    x.plusSeqLength();
                    text_seqLength.setText(String.valueOf("Seq Length: " + x.getSeqLength()));
                    barChart.highlightValue(x.getChosen(),0);
                    barDataSet = new BarDataSet(x.barEntries,"");
                    refreshBarDataSet_DT();
                }
                else {
                    y.plusSeqLength();
                    text_seqLength.setText(String.valueOf("Seq Length: " + y.getSeqLength()));
                    barChart.highlightValue(y.getChosen(),0);
                    barDataSet = new BarDataSet(y.barEntries,"");
                    refreshBarDataSet_WH();
                }
                BarData theData = new BarData(barDataSet);
                barChart.setData(theData);
                barChart.notifyDataSetChanged();
                barChart.invalidate();
            }
        });

        seekBar_value.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(radioBtn_DT.isChecked()) {
                    x.setChosenValue(x.getChosen(),seekBar.getProgress());
                    text_value.setText(x.getSeqValueNames(seekBar.getProgress()));
                }
                else{
                    y.setChosenValue(y.getChosen(),seekBar.getProgress());
                    text_value.setText(y.getSeqValueNames(seekBar.getProgress()));
                }
                barChart.notifyDataSetChanged();
                barChart.invalidate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                vibe.vibrate(20);
                if(radioBtn_DT.isChecked()) {
                    text_value.setText(x.getSeqValueNames(seekBar.getProgress()));
                    x.setChosenValue(x.getChosen(), seekBar.getProgress());
                }
                else{
                    text_value.setText(y.getSeqValueNames(seekBar.getProgress()));
                    y.setChosenValue(y.getChosen(), seekBar.getProgress());
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(radioBtn_DT.isChecked()) {
                    text_value.setText(x.getSeqValueNames(x.getChosenValue(x.getChosen())));
                    x.setChosenValue(x.getChosen(), seekBar.getProgress());
                }
                else{
                    text_value.setText(y.getSeqValueNames(y.getChosenValue(y.getChosen())));
                    y.setChosenValue(y.getChosen(), seekBar.getProgress());
                }
            }
        });

        radioBtn_Deep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);
                radioBtn_Shallow.setChecked(false);
            }
        });

        radioBtn_Shallow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);
                radioBtn_Deep.setChecked(false);
            }
        });

        barChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Highlight[] highlight;
                highlight = barChart.getHighlighted();

                if(highlight!=null){

                    if(radioBtn_DT.isChecked()) {
                        x.setChosen((int) highlight[0].getX());
                        text_selected.setText(String.valueOf("Selected: " + (x.getChosen()+1)));
                    }
                    else{
                        y.setChosen((int) highlight[0].getX());
                        text_selected.setText(String.valueOf("Selected: " + (y.getChosen()+1)));
                    }
                }

            }
        });

    }
}
