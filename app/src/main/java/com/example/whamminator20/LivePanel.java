package com.example.whamminator20;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ToggleButton;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;

public class LivePanel extends AppCompatActivity {

    TempSliderControler temp = new TempSliderControler(); //thats a test comment
    BarChart barChart;
    BarDataSet barDataSet;
    MyBarChart barChartEdit = new MyBarChart();

    public void refreshBarDataSetTemp(){
        barDataSet.setColor(getResources().getColor(R.color.colorAccent));
        barDataSet.setBarShadowColor(getResources().getColor(R.color.darkgrey));
        barDataSet.setHighLightColor(getResources().getColor(R.color.colorPrimary));
        barDataSet.setHighLightAlpha(255);
        barDataSet.setDrawValues(false);
        barDataSet.setHighlightEnabled(false);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_panel);
        final Vibrator vibe = (Vibrator) LivePanel.this.getSystemService(Context.VIBRATOR_SERVICE);

        Intent intent = getIntent();
        temp.setSeqLength(intent.getIntExtra(CreatorPanel.EXTRA_SEQLENGTH,1));
        temp.setTempEntriesArray(intent.getIntArrayExtra(CreatorPanel.EXTRA_ENTRIES));

        final Button btn_live = (Button)findViewById(R.id.btn_live2);
        final ToggleButton btn_startstop = (ToggleButton) findViewById(R.id.btn_startstop);
        EditText tempo_display2 = (EditText)findViewById(R.id.tempo_display2);
        final Button btn_tempoNext = (Button)findViewById(R.id.btn_tempoNext);
        barChart=(BarChart)findViewById(R.id.bar_chart_temp);

        temp.fillArray();
        barDataSet = new BarDataSet(temp.barEntries,"");
        BarData theData = new BarData(barDataSet);
        barChart.setData(theData);
        barChart = barChartEdit.EditMyBarChart(barChart);
        barChart.setBackgroundColor(getResources().getColor(R.color.background_color));
        barChart.highlightValue(0,0);
        refreshBarDataSetTemp();

        tempo_display2.setText("0");
        btn_startstop.setChecked(false);

        btn_live.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);
                finish();
            }
        });

        btn_startstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);

                if (btn_startstop.isChecked()) {
                    btn_startstop.setBackground(getResources().getDrawable(R.drawable.btn_live_stop));
                    btn_tempoNext.setBackground(getResources().getDrawable(R.drawable.btn_temponext_liveon));

                } else {
                    btn_startstop.setBackground(getResources().getDrawable(R.drawable.btn_live_start));
                    btn_tempoNext.setBackground(getResources().getDrawable(R.drawable.btn_temponext_liveoff));
                }
            }
        });

        btn_tempoNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibe.vibrate(20);

            }
        });

    }
}
