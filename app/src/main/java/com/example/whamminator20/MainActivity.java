package com.example.whamminator20;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Vibrator vibe = (Vibrator) MainActivity.this.getSystemService(Context.VIBRATOR_SERVICE);

        Button btnWhammyLogo = (Button) findViewById(R.id.btn_logoWhamminator);
        TextView textView_MIDISupport = (TextView) findViewById(R.id.textView_IsMIDISupported);

        if (getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_MIDI)) {
            textView_MIDISupport.setText("MIDI is supported on this device");
        }
        else { textView_MIDISupport.setText("MIDI is not supported on this device :(");
        }

        btnWhammyLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(), CreatorPanel.class);
                startActivity(startIntent);
                vibe.vibrate(20);

            }
        });

    }
}
