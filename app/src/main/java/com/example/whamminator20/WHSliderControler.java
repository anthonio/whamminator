package com.example.whamminator20;

import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class WHSliderControler {
    private int seqLength =8;
    private int chosenBar =0;
    private int[] seqValuesArray;
    private String[] seqVNamesArray = new String[] {"+2/+3","+b3/+3","+3/+4","+4/+5","+5/+6","+5/+5","-4/-3","-5/-4","+/-OCT","OFF","DIVE","-2OCT","-OCT","-5TH","-4TH","-2ND","+4TH","+5TH","+O+D","+2OCT"};
    ArrayList<BarEntry> barEntries = new ArrayList<>(16);

    public int[] fillValuesArray(){
        seqValuesArray=new int[seqLength];
        for (int i=0; i<seqLength; i++){
            seqValuesArray[i]=getChosenValue(i);
        }
        return seqValuesArray;
    }

    public int getSeqLength(){
        return seqLength;
    }

    public void setSeqLength(int sekw){
        seqLength =sekw;
    }

    public void plusSeqLength(){
        if(seqLength <16){
            seqLength++;
            barEntries.add(new BarEntry(seqLength-1,9));
        }
        else{ seqLength=16; }

    }

    public void minusSeqLength(){
        if(seqLength >1){
            seqLength--;
            barEntries.remove(seqLength);
        }
        else { seqLength =1; }
    }

    public int getChosen(){
        return chosenBar;
    }

    public void setChosen(int wybor){
        chosenBar =wybor;
    }

    public void plusChosen(){
        if (chosenBar < seqLength-1){
            chosenBar++;
        }
        else {
            chosenBar=0;}
    }

    public void minusChosen(){
        if (chosenBar >1){
            chosenBar--;
        }
        else {
            chosenBar = seqLength-1;}
    }

    /*public void setChosenValue(int chosen, int value){
        seqValuesArray[chosen]=value;
    }

    public int getChosenValue(int chosen){
        return seqValuesArray[chosen];
    }*/

    public String getSeqValueNames(int value){
        return seqVNamesArray[value];
    }

/*
    public int getChosenValueScaled(int value){
        return (value*100)/(19);
    }
*/

//-------------------------------------------------------------------------//

    public void fillArray(){
        for (int i=0; i<seqLength; i++){
            barEntries.add(new BarEntry(i,9));
        }
    }

    public void setChosenValue(int chosen, int value){
        barEntries.set(chosen,new BarEntry(chosen,value));
    }

    public int getChosenValue(int chosen){
        return (int) barEntries.get(chosen).getY();
    }

}
